var mySwiper = new Swiper('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    autoplay: {
        delay: 5000,
    },
})
var profileSlider = new Swiper('.swiper-container2', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
})
var feedbackSlider = new Swiper('.feedback__slider', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,

    // If we need pagination
    pagination: {
        el: '.swiper-pagination',
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
})

// window.addEventListener('scroll', function() {
//     if(pageYOffset >= 20){
//         document.getElementById('navbar').classList.add('navbar__not-at-top')
//     }else{
//         document.getElementById('navbar').classList.remove('navbar__not-at-top')
//
//     }
// });
let categories = document.getElementsByClassName('work__category')
Array.from(categories).forEach(function(element) {
    element.addEventListener('click', function (e){
        e.preventDefault()
        console.log(e.target.getAttribute('data-filter'))
        let filter = e.target.getAttribute('data-filter')
        $.each($('.work__image'),function (index,value){
            console.log(this)
            if($(this).attr('data-work')!==filter){
                $(this).hide()
            }else{
                $(this).fadeIn('slow')
            }
            if(filter==='all'){
                $(this).fadeIn('slow')
            }
        })

    });
});
